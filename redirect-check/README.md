# Redirect check

Automatically checking redirects using a CSV file.
Delimiter used is `;`

### Usage:

```bash
./redirect-check.php redirects.csv
```
### CSV format:
`{URL};{DESTINATION};{EXPECTED STATUS CODE}`

### Example CSV: (All fields required)
```
http://vol.at;http://www.vol.at;301
http://vol.at;http://www.vol.at/;302
http://vol.at;http://www.vol.at/oops;301
```

### Example output:
![Output](docs/redirect.png)
