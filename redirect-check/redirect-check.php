#!/usr/bin/php
<?php

const GREEN_COLOR = "\033[32m ";
const RED_COLOR = "\033[31m ";
const WHITE_COLOR = "\033[0m ";

if (!file_exists($argv[1])) {
    echo PHP_EOL . PHP_EOL . 'USAGE: ' . $argv[0] . ' file.csv' . PHP_EOL . PHP_EOL;
    exit(0);
}

if (false !== ($handle = fopen($argv[1], "r"))) {
    while (false !== ($data = fgetcsv($handle, 1000, ";"))) {
        $result = shell_exec('curl -s -I ' . $data[0]);
        $headers = http_parse_headers($result);

        $codeParts = explode(' ',$headers[0]);
        $locationColor = GREEN_COLOR;
        if ($headers['Location'] !== $data[1] && $headers['Location'] !== $data[1] . '/') {
            $locationColor = RED_COLOR;
        }
        $codeColor = GREEN_COLOR;
        if ($codeParts[1] !== $data[2]) {
            $codeColor = RED_COLOR;
        }

        echo WHITE_COLOR . $data[0] . ' -> '
            . $locationColor . $headers['Location'] .
            WHITE_COLOR . '(' . $data[1] . ')' .
            ' :: ' . $codeColor . $codeParts[1] .
            WHITE_COLOR . '(' . $data[2] . ')' . PHP_EOL;
    }
    fclose($handle);
}

/**
 * @param string $raw_headers
 *
 * @return array
 */
function http_parse_headers($raw_headers)
{
    $headers = array();
    $key = '';

    foreach (explode("\n", $raw_headers) as $i => $h) {
        $h = explode(':', $h, 2);

        if (isset($h[1])) {
            if (!isset($headers[$h[0]]))
                $headers[$h[0]] = trim($h[1]);
            elseif (is_array($headers[$h[0]])) {
                $headers[$h[0]] = array_merge($headers[$h[0]], array(trim($h[1])));
            } else {
                $headers[$h[0]] = array_merge(array($headers[$h[0]]), array(trim($h[1])));
            }

            $key = $h[0];
        } else {
            if (substr($h[0], 0, 1) == "\t")
                $headers[$key] .= "\r\n\t" . trim($h[0]);
            elseif (!$key)
                $headers[0] = trim($h[0]);
            trim($h[0]);
        }
    }

    return $headers;
}


?>

